#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    #When this node1 comes up it will send a POST request to it's next node2
    #On receiving POST reqest node2 will zip the file and send that file to node1's predefiend directory
    urls = "http://192.168.97.3:7890/bucket/handoff/"
    keyValue = {'anyrandomvalue':'anyrandomvalue'}     
    resrec = requests.post(url = urls, data = keyValue) #program will auto wait till it will receive response
    restextvalue=resrec.text
    print(restextvalue)
    #On receiving tar file from node 2, node 1 will untar it and store it to predefined path
    cmd= 'tar -xvf /home/abhayrajj/dynamoDb/recvhandoff/handoff.tar.gz --strip 4 --directory /home/abhayrajj/dynamoDb/dynamo/bucketData'
    returnValue=os.system(cmd) #auto wait till some value is returned

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dynamo.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
