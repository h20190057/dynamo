# Generated by Django 2.2.6 on 2019-10-27 13:15

import bucket.models
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bucket',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('bucket_name', models.CharField(max_length=50)),
                ('node_name', models.CharField(max_length=50)),
                ('bucket_path', models.CharField(max_length=50)),
                ('timestamp', models.DateField()),
                ('bucket_version', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file_name', models.CharField(max_length=50)),
                ('file_path', models.FileField(null=True, upload_to=bucket.models.data_directory_path)),
                ('file_extension', models.CharField(max_length=10)),
                ('timestamp', models.DateField()),
                ('file_Version', models.IntegerField()),
                ('bucket_nvmber', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bucket.Bucket')),
            ],
        ),
    ]
