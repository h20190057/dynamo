from django.shortcuts import render
from django.core.cache import cache
import os, json, imp, shutil
from dynamo import settings
from django.http import JsonResponse, HttpResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
import datetime

 
@csrf_exempt
def create_bucket(request):
    # print(request.method)
    # now = datetime.datetime.now()
    # html = "<html><body>It is now %s.</body></html>" % now
    # return HttpResponse(html)
    if request.method == 'POST':
        bucketName = request.POST['bucketName']
        # print(bucketName)
        path =settings.MEDIA_ROOT+'//'+bucketName
        print(path)
        if not os.path.exists(path):
            os.makedirs(path)
            response = {
                'sucess' : True,
                'vector clock' : 'TBA',
                'node number' : 'Node 1'
            }
        else:
            # bukcet already exists
            response = {
                'sucess' : False,
                'error' : 'bukcet already exists',
                'node number' : 'Node 1'
            } 
    return JsonResponse(response)

@csrf_exempt
def delete_bucket(request):
    if request.method == 'POST':
        bucketName = request.POST['bucketName']
        path =settings.MEDIA_ROOT+'//'+bucketName
        if not os.path.exists(path):
            response = {
                'sucess' : False,
                'error' : 'Bucket does not exists',
                'node number' : 'Node 1'
            }
        else:
            # bukcet exists
            # os.rmdir(path)
            shutil.rmtree(path)
            response = {
                'sucess' : True,
                'vector clock' : 'TBA',
                'node number' : 'Node 1'
            }
    return JsonResponse(response)


@csrf_exempt
def create_file(request):
    if request.method == 'POST':
        bucketName = request.POST['bucketName']
        fileName = request.POST['fileName']
        fileContent = request.POST['fileContent']

        bucketpath =settings.MEDIA_ROOT+'//'+bucketName
        filepath =settings.MEDIA_ROOT + '//' +bucketName + '//' +fileName
        if not os.path.exists(bucketpath):
            response = {
                'sucess' : False,
                'error' : 'bukcet does not exist',
                'node number' : 'Node 1'
            }
        else:
            if os.path.isfile(filepath):
                # file already exists
                # update vector clock in db
                os.remove(filepath)
            
            file = open(filepath,"a")
            file.write(fileContent)
            response = {
                'sucess' : True,
                'vector clock' : 'TBA',
                'node number' : 'Node 1'
            }
    return JsonResponse(response)

@csrf_exempt
def delete_file(request):
    if request.method == 'POST':
        bucketName = request.POST['bucketName']
        fileName = request.POST['fileName']

        bucketpath =settings.MEDIA_ROOT+'//'+bucketName
        filepath =settings.MEDIA_ROOT + '//' +bucketName + '//' +fileName
        if not os.path.exists(bucketpath):
            response = {
                'sucess' : False,
                'error' : 'bukcet does not exist',
                'node number' : 'Node 1'
            }
        else:
            if not os.path.isfile(filepath):
                response = {
                'sucess' : False,
                'error' : 'file does not exist',
                'node number' : 'Node 1'
            }  
                
            else:
                os.remove(filepath)
                response = {
                    'sucess' : True,
                    'vector clock' : 'TBA',
                    'node number' : 'Node 1'
                }
    return JsonResponse(response)

@csrf_exempt
def handoff(request):
 if request.method=='POST':
    #check if there is some data to handoff 
    if not len(os.listdir('/home/abhayrajj/dynamoDb/tohandoff'))==0:
     cmd1='tar -zcvf /home/abhayrajj/dynamoDb/handoff.tar.gz /home/abhayrajj/dynamoDb/tohandoff'
     cmd2='scp /home/abhayrajj/dynamoDb/handoff.tar.gz abhayrajj@192.168.97.4:/home/abhayrajj/dynamoDb/recvhandoff'
     os.system(cmd1)
     y=os.system(cmd2) #auto wait till cmd2 return some value
     response={
       'Handeoff': 'Success',       
       'Souce Node': 'node 1',
       'destination Node' : 'node 2'
      }
    else:
     response={
       'Handeoff': 'No Data',       
       'Souce Node': 'node 1',
       'destination Node': 'node 2'
      }


 return JsonResponse(response)




# Pooja's old code
    #     if 'file' in request.FILES:
    #         attachment_file = request.FILES['file']
            
    #         attachment = Attachment()            
    #         attachment.file.save(
    #             attachment_file.name, content=attachment_file, save=True
    #         )
    #         email.attachments.add(attachment)

    #     status = 'Mail send successfully!'
    #     return render(request, "mailer.html", {'status': status})
    # else:
    #     message = request.GET['message']

    #     return render(request, "mailer.html", {'message': message, 'status': ''})

    
    #         path =settings.MEDIA_ROOT+"//"+name+".phrazor.com/data/"+name+"//"
    #         print(path)
    #         filepath=os.path.join(path,"test.py")
    #         if not os.path.exists(path):
    #             os.makedirs(path)
    #         open(filepath,"a")
