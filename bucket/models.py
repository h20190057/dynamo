from django.db import models

def data_directory_path(instance, file_name):
    # file will be uploaded to MEDIA_ROOT
    return '{bucketname}/{name}'.format(
        name = file_name,
        bucketname = instance.bucket_number.bucket_name
    )

# Create your models here.
class Bucket(models.Model):
	bucket_name = models.CharField(max_length=50)
	node_name = models.CharField(max_length=50)
	bucket_path = models.CharField(max_length=50)
	timestamp = models.DateField()
	bucket_version = models.IntegerField()

class File(models.Model):
	bucket_nvmber = models.ForeignKey(Bucket, on_delete=models.CASCADE)
	file_name = models.CharField(max_length=50)
	file_path = models.FileField(upload_to=data_directory_path, null=True)
	file_extension = models.CharField(max_length=10)
	timestamp = models.DateField()
	file_Version = models.IntegerField()

