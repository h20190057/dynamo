from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from .views import create_bucket, create_file, delete_bucket, delete_file,handoff
from django.views.generic.base import TemplateView


urlpatterns = [
    path(
    	'create_bucket/',
    	create_bucket,
    	name='create_bucket'
    	),
    path(
    	'delete_bucket/',
    	delete_bucket,
    	name='create_bucket'
    	),

    path(
    	'create_file/',
    	create_file,
    	name='create_file'
    	),
    path(
    	'delete_file/',
    	delete_file,
    	name='create_file'
    	),
	path(
		'handoff/',
		 handoff,
		 name='handoff'
		 )
    ]
